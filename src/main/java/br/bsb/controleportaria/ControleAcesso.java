/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.bsb.controleportaria;

import br.bsb.controleportaria.modelo.Pessoa;
import br.bsb.controleportaria.repository.PessoaRepository;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import sun.awt.image.ToolkitImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author 1540 LITE
 */
public class ControleAcesso extends javax.swing.JPanel {

    private PessoaRepository pessoaRepository;

    public ControleAcesso() {
        initComponents();
    }

    public ControleAcesso(PessoaRepository pessoaRepository) {
        this();
        this.pessoaRepository = pessoaRepository;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            javax.swing.UIManager.LookAndFeelInfo[] installedLookAndFeels = javax.swing.UIManager.getInstalledLookAndFeels();
            for (int idx = 0; idx < installedLookAndFeels.length; idx++) {
                if ("Windows".equals(installedLookAndFeels[idx].getName())) {
                    javax.swing.UIManager.setLookAndFeel(installedLookAndFeels[idx].getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ControleAcesso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ControleAcesso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ControleAcesso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ControleAcesso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                javax.swing.JFrame frame = new javax.swing.JFrame("Controle de Acesso");
                frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                frame.getContentPane().add(new ControleAcesso());
                frame.pack();
                frame.setVisible(true);
            }
        });
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupMotivoEntrada = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        nomeTextField = new javax.swing.JTextField();
        foto = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        rgTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cpfTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        visitanteRadioButton = new javax.swing.JRadioButton();
        servicoRadioButton = new javax.swing.JRadioButton();
        salvarButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        apartamentoTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        blocoTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        dataJDateChoose = new com.toedter.calendar.JDateChooser();
        tirarFotoButton = new javax.swing.JButton();
        pesquisarButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Nome");

        nomeTextField.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        nomeTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomeTextFieldActionPerformed(evt);
            }
        });

        foto.setBackground(new java.awt.Color(255, 51, 153));
        foto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        foto.setText("Foto");
        foto.setToolTipText("Foto");
        foto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("RG");

        rgTextField.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        rgTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rgTextFieldActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("CPF");

        cpfTextField.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cpfTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cpfTextFieldActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Motivo entrada");

        buttonGroupMotivoEntrada.add(visitanteRadioButton);
        visitanteRadioButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        visitanteRadioButton.setText("Visitante");
        visitanteRadioButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                visitanteRadioButtonStateChanged(evt);
            }
        });
        visitanteRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visitanteRadioButtonActionPerformed(evt);
            }
        });

        buttonGroupMotivoEntrada.add(servicoRadioButton);
        servicoRadioButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        servicoRadioButton.setText("Serviço");
        servicoRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                servicoRadioButtonActionPerformed(evt);
            }
        });

        salvarButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        salvarButton.setText("Salvar");
        salvarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarButtonActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Apartamento");

        apartamentoTextField.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Bloco");

        blocoTextField.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        blocoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blocoTextFieldActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Data");

        dataJDateChoose.setDateFormatString("dd/MM/yyyy");

        tirarFotoButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tirarFotoButton.setText("Tirar foto");
        tirarFotoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tirarFotoButtonActionPerformed(evt);
            }
        });

        pesquisarButton.setText("Pesquisar");
        pesquisarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pesquisarButtonActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton1.setText("Limpar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(salvarButton)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButton1)
                                                .addGap(0, 484, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel5)
                                                        .addComponent(apartamentoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel6)
                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                        .addComponent(blocoTextField))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel7)
                                                        .addComponent(dataJDateChoose, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(11, 11, 11))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel3)
                                                        .addComponent(cpfTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel2)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(rgTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(pesquisarButton)))
                                                .addGap(119, 119, 119))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(nomeTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addComponent(jLabel1)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 257, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addComponent(jLabel4)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(visitanteRadioButton)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(servicoRadioButton)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(foto, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(tirarFotoButton))
                                .addGap(27, 27, 27))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(foto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel2)
                                                        .addComponent(jLabel3))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(rgTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(cpfTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(pesquisarButton))
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(nomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                                                .addComponent(jLabel4)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(visitanteRadioButton)
                                                        .addComponent(servicoRadioButton))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel5)
                                                        .addComponent(jLabel6)
                                                        .addComponent(jLabel7))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(apartamentoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(blocoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(dataJDateChoose, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(33, 33, 33)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(tirarFotoButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(salvarButton)
                                        .addComponent(jButton1))
                                .addGap(27, 27, 27))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void nomeTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomeTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomeTextFieldActionPerformed

    private void rgTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rgTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rgTextFieldActionPerformed

    private void cpfTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cpfTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cpfTextFieldActionPerformed

    private void visitanteRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visitanteRadioButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_visitanteRadioButtonActionPerformed

    private void servicoRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_servicoRadioButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_servicoRadioButtonActionPerformed

    private void visitanteRadioButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_visitanteRadioButtonStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_visitanteRadioButtonStateChanged

    private void tirarFotoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tirarFotoButtonActionPerformed

        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(WebcamResolution.VGA.getSize());

        webcam.open();

        // get image
        BufferedImage image = webcam.getImage();

        // save image to PNG file
        //ImageIO.write(image, "PNG", new File("test.png"));
        ImageIcon fotoIcon = new ImageIcon(image.getScaledInstance(foto.getWidth(), foto.getHeight(), 1));

        foto.setIcon(fotoIcon);        // TODO add your handling code here:

        webcam.close();
    }//GEN-LAST:event_tirarFotoButtonActionPerformed

    private void blocoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blocoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_blocoTextFieldActionPerformed

    private void salvarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarButtonActionPerformed

        Pessoa pessoaACadastrar = new Pessoa();

        pessoaACadastrar.setUltimoAcesso(new Date());
        pessoaACadastrar.setNome(nomeTextField.getText());
        pessoaACadastrar.setRg(rgTextField.getText());
        pessoaACadastrar.setCpf(cpfTextField.getText());
        pessoaACadastrar.setApartamento(Long.valueOf(apartamentoTextField.getText()));
        pessoaACadastrar.setBloco(blocoTextField.getText());
        pessoaACadastrar.setData(dataJDateChoose.getDate());

        for (Enumeration<AbstractButton> buttons = buttonGroupMotivoEntrada.getElements(); buttons.hasMoreElements(); ) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                pessoaACadastrar.setMotivoVisita(button.getText());
            }
        }

        try {
            ImageIcon icon = (ImageIcon) foto.getIcon();

            BufferedImage bimage = ((ToolkitImage) icon.getImage()).getBufferedImage();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bimage, "jpg", baos);
            baos.flush();
            pessoaACadastrar.setFoto(baos.toByteArray());
            baos.close();
        } catch (IOException ex) {
            Logger.getLogger(ControleAcesso.class.getName()).log(Level.SEVERE, null, ex);
        }


        Pessoa pessoaCadastrada = pessoaRepository.save(pessoaACadastrar);
        System.out.println("Pessoa cadastrada com sucesso id " + pessoaCadastrada.getId());
        JOptionPane.showMessageDialog(null, "Pessoa cadastrada com sucesso id " + pessoaCadastrada.getId(), "Cadastro de Usuário", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_salvarButtonActionPerformed

    private void pesquisarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pesquisarButtonActionPerformed
        String cpf = cpfTextField.getText();
        String rg = rgTextField.getText();

        Pessoa pessoa = pessoaRepository.findByCpf(cpf).orElse(null);

        if (pessoa == null) {
            JOptionPane.showMessageDialog(null, "Pessoa não encontrada", "Pesquisa de Usuário", JOptionPane.WARNING_MESSAGE);
            limpar();
            return;
        }

        nomeTextField.setText(pessoa.getNome());
        rgTextField.setText(pessoa.getRg());
        cpfTextField.setText(pessoa.getCpf());
        // cpfTextField.setText(pessoa.);
        apartamentoTextField.setText(pessoa.getApartamento().toString());
        blocoTextField.setText(pessoa.getBloco());
        dataJDateChoose.setDate(pessoa.getData());

        if (pessoa.getMotivoVisita().equals(visitanteRadioButton.getText())) {
            visitanteRadioButton.setSelected(true);
        }
        if (pessoa.getMotivoVisita().equals(servicoRadioButton.getText())) {
            servicoRadioButton.setSelected(true);
        }

        try {
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(pessoa.getFoto()));
            ImageIcon fotoIcon = new ImageIcon(image.getScaledInstance(foto.getWidth(), foto.getHeight(), 1));

            foto.setIcon(fotoIcon);        // TODO add your handling code here:
        } catch (IOException e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_pesquisarButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        limpar();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField apartamentoTextField;
    private javax.swing.JTextField blocoTextField;
    private javax.swing.ButtonGroup buttonGroupMotivoEntrada;
    private javax.swing.JTextField cpfTextField;
    private com.toedter.calendar.JDateChooser dataJDateChoose;
    private javax.swing.JLabel foto;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField nomeTextField;
    private javax.swing.JButton pesquisarButton;
    private javax.swing.JTextField rgTextField;
    private javax.swing.JButton salvarButton;
    private javax.swing.JRadioButton servicoRadioButton;
    private javax.swing.JButton tirarFotoButton;
    private javax.swing.JRadioButton visitanteRadioButton;
    // End of variables declaration//GEN-END:variables


    public void limpar() {
        nomeTextField.setText(null);
        rgTextField.setText(null);
        cpfTextField.setText(null);
        apartamentoTextField.setText(null);
        blocoTextField.setText(null);
        dataJDateChoose.setDate(null);
        foto.setIcon(null);

        buttonGroupMotivoEntrada.clearSelection();
    }
}
