/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.bsb.controleportaria.modelo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pessoa")
public class Pessoa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "rg")
    private String rg;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "apartamento")
    private Long apartamento;

    @Column(name = "bloco")
    private String bloco;

    @Column(name = "data")
    private Date data;

    @Column(name = "motivo_visita")
    private String motivoVisita;

    @Lob
    @Column(name = "foto", columnDefinition="BLOB")
    private byte[] foto;

    @Column(name = "ultimo_acesso")
    private Date ultimoAcesso;

}
