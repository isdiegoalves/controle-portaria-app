package br.bsb.controleportaria.repository;

import br.bsb.controleportaria.modelo.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long>, QueryByExampleExecutor<Pessoa> {

    public Optional<Pessoa> findByCpf(String cpf);

    public Optional<Pessoa> findByRg(String rg);
}